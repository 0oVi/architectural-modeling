# Системная инженерия. <br/> Архитектурное моделирование компьютерных систем

## Лекция 6

### OMG Essence. Архитектурное документирование

Пенской А.В., 2022

----

### План лекции

- Примеры ошибок архитектурного/системного уровня
- SEMAT. OMG Essence
- Архитектурное документирование

---

<div class="row"><div class="col">

## Примеры ошибок архитектурного/системного уровня

Выше были теоретические рассуждения.

Теперь несколько примеров из практики.

</div><div class="col">

![](figures/geek-and-poke-fail.png)<!-- .element: height="600px" -->

</div></div>

----

### Пример проблем с операционным окружением

![](figures/controller-right2left.png)

----

Подсказка:

![](figures/controller-right2left-2.png)

Notes: На столе разработчика -- силовые линии лучше увести со стола вверх. В шкафу -- вниз, т.к. техника безопасности.

----

### Пример с коммуникацией между стадиями жизненного цикла

![](figures/angle-marks.png)

----

Подсказка:

![](figures/angle-marks-2.png)

Notes: План: вырезать на лазере. Синие точки -- отверстия в качестве маркировки уголка. Делали руками, замучились отмерять.

----

### Пример о следовании стандартам

Из описания формата JSON:

<div class="row"><div class="col">

```text
number 
    integer fraction exponent 

integer 
    digit
    onenine digits 
    '-' digit 
    '-' onenine digits 

digits 
    digit 
    digit digits 

digit 
    '0' 
    onenine 
```

</div><div class="col">

```text
onenine 
    '1' . '9' 

fraction 
    "" 
    '.' digits
    
exponent 
    "" 
    'E' sign digits 
    'e' sign digits
```

</div></div>

Что такое number?

----

#### Что такое number в JSON?

`number` может быть: short, int, long, big-int, float, double

#### Контекст

- JavaScript -- старая экосистема, разработанная для автоматизации веба на коленке.
    - Следствие: работа с `number` как с `long` в большинстве решений.
- Haskell -- язык, сделанный любителями и профессионалами от математики.
    - Следствие 1: есть `Integer` без ограничения диапазона значений.
    - Следствие 2: библиотека Aeson (стандарт для работы с JSON) интерпретирует `number` как `Integer`.
- Crypto & Blockchain mess -- активное использование ключей.
    - Следствие: числа в 256 бит не предел.

----

#### Проблема

- Передача JSON из Haskell в веб.
- Сериализация `Integer` в `number` формирует очень длинное число, которое парсится в `long`.
- Hash перестаёт совпадать.

#### Решение

- предобработка текста JSON с заменой всех больших `number` на специальную строку;
- постобработка структуры с JSON с возвращением всех `number` к истинным значениям.

---

## SEMAT. OMG Essence

SEMAT
: Software Engineering Method and Theory

OMG
: Object Management Group

<br/>

Essence
: сущнось, суть

<div class="row"><div class="col">

![](figures/ivar-jacobson.png)<!-- .element: height="300px" -->

</div><div class="col">

![](figures/essence-book.png)<!-- .element: height="300px" -->

</div></div>

----

### SEMAT call for action

The software industry, which is perhaps the largest sector in the world, has become extremely successful. However, software engineering as a discipline is still challenged by similar problems as it was at its beginning.

The software industry to some extent is like the fashion industry in the way that we keep chasing fads or silver bullets, but keep ignoring the basic, fundamental things in our discipline.

Research topics and education curricula at universities are generally so remote from what the industry wants and needs.

Semat addresses the “human” side of software development as well as the technical side because after all, it is people who develop software, not methods and tools.

We suggested that we needed to refound software engineering based on a solid theory, proven principles and best practices that: Include “a kernel of widely-agreed elements, extensible for specific uses”.

----

### Essence Method architecture

<div class="row"><div class="col">

- Совокупность практик и основных компонент для конкретной задачи
- Подручный, повторяемый способ достижения цели для заданного аспекта
- Основные компоненты
- DSL для методологий

</div><div class="col">

![](figures/essence-method.png)

</div></div>

---

## Essence. Language

### Abstract-Level Progress Health Attribute

<div class="row"><div class="col">

Alphas
: The Things to Work With

- capture the key concepts involved in software engineering;
- allow the progress and health of any software engineering endeavor to be tracked and assessed;
- and provide the common ground for the definition of software engineering methods and practices.

</div><div class="col">

![](figures/essence-alphas.png)

</div></div>

----

![](figures/essence-alpha-stages.png)

----

![](figures/essence-language.png)

----

### Sub-Alphas and Work Products

![](figures/essence-sub-alphas.png)

----

### Vee диаграмма (ЖЦ проекта)

Multi-phase Waterfall Practice Activities Flow

![](figures/essence-v-diagram.png)

---

## Essence. Kernel

### Области интересов <br/>(The Three Areas of Concern)

![](figures/essence-area-of-concern.png)

----

### Alphas of Software System

![](figures/essence-alphas-scheme.png)

----

### Competencies: The Abilities Needed

<div class="row"><div class="col">

1. Assists
2. Applies
3. Masters
4. Adapts
5. Innovates

</div><div class="col">

![](figures/essence-competence.png)

</div></div>

----

### Activity Spaces: The Things to Do

![](figures/essence-activities.png)

----

### SEMAT Kernel Cards

![](figures/essence-cards.png)

----

### A Waterfall lifecycle

![](figures/essence-watter-fall.png)

----

<div class="row"><div class="col">

![](figures/method-agile-vs-waterfall.png)

</div><div class="col">

![](figures/method-waterfall.png)

</div><div class="col">

![](figures/method-made-with-agile.png)

</div></div>

---

## Архитектурное документирование

<div class="row"><div class="col">

Задачи:

1. Architecture serves as a means of education.
2. Architecture serves as a primary vehicle for communication among stakeholders.
3. Architecture serves as the basis for system analysis and construction.

</div><div class="col">

![](figures/documenting-arch-book.png)

</div></div>

----

### Архитектурный стиль

<div class="row"><div class="col">

Архитектурный стиль (или архитектурный framework)
: соглашения, принципы, практики для описания архитектуры, созданной для конкретной области применения и/или заинтересованных сторон (перевод ISO 42010).

</div><div class="col">

![](figures/capella.png)

Качество архитектурных решений выражается в совокупном снижении затрат заинтересованных сторон (stakeholders) на протяжении всего жизненного цикла системы.

</div></div>

----

### Architectural specification

<div class="row"><div class="col">

Architecture
: is fundamental concepts or properties of a system in its environment embodied in its elements, relationships, and in the principles of its design and evolution [ISO 42010].

Architectural specification
: work product used to express an architecture.

The most popular type of architectural specification describes selected layer of the system and we call it "horizontal".

</div><div class="col">

![](figures/hor-arch-desc.png) <!-- .element: height="250px" -->

![](figures/hor-arch-desc-processes.png) <!-- .element: height="250px" -->

</div></div>

----

### Подход к рассмотрению архитектуры

<div class="row"><div class="col">

![](figures/platform-based-design.png)

Горизотальный

</div><div class="col">

![](figures/vertical-horizontal-arch.png)

Вертикальный

</div></div>

----

### Architectural specification (vertical)

<div class="row"><div class="col">

1. Specification of layers and their relationship.
2. In opposite to "horizontal" specification we offer "vertical" architectural specification.
3. Relationship -- evaluation or actualisation.

</div><div class="col">

![](figures/osi-model.png)

</div></div>
