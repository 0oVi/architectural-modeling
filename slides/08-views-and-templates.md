# Системная инженерия. <br/> Архитектурное моделирование компьютерных систем

## Лекция 8

### Архитектурные представления. Шаблоны

Пенской А.В., 2022

----

### План лекции

- Views
- Templates

---

## Важные аспекты любой системы

- Context Diagram
- Interface Diagram
- Behavior Diagram
- Уровневые представления

----

### Context Diagram

![](figures/context-diagram.png)

----

### Interface Diagram

<div class="row"><div class="col">

![](figures/arch-interface-documentation-1.png)

</div><div class="col">

![](figures/arch-interface-documentation-2.png)

</div></div>

---

### Behavior Diagram

![](figures/behavior-1.png)

----

![](figures/behavior-2.png)

----

![](figures/behavior-3.png) <!-- .element: class="fullscreen" -->

----

![](figures/behavior-4.png)

----

![](figures/behavior-5.png)

----

![](figures/behavior-6.png)

----

![](figures/behavior-7.png)

----

![](figures/behavior-8.png) <!-- .element: class="fullscreen" -->

---

## Уровневая организация

![](figures/vertical-horizontal-arch.png)

----

### Системно-иерархический стиль

<div class="row"><div class="col">

- Система -- единство функционального места и конструкции (обеспечивающей функциональные требования).
- Работа с жизненным циклом построена на концепциях `whole-life-individual` и `temporal-part` по ISO 15926.
- Трассировка требования.
- Отображение функциональных мест на всём жизненном цикле.
- Отображение модульности и многофункциональности.

</div><div class="col">

![](figures/burger-diagram.png)
![](figures/burger-diagram-2.png)

</div></div>

----

### Граф актуализации

<div class="row"><div class="col">

- Represent the structure of computational process actualisation.
- One of axis of design space.
- Oriented acyclic graph, where vertex -- translator, arcs -- any transfers between translator.
- Unified computation process representation for
    - Design-time and run-time phases.
    - Software and Hardware component.

</div><div class="col">

![](figures/actualization-graph.png)

</div></div>

----

### Модифицированный граф актуализации

<div class="row"><div class="col">

- Вредставления ВС в рамках различных ВПл вне зависимости от стадии жизненного цикла.
- Ориентированный нециклический граф, где вершины -- спецификации, используемые в ВсС, рёбра -- трансляции между ними.
- Анализ инструментальных цепочек и представлений процесса.
- Последовательная актуализация спецификаций в физический процесс не позволяет включить в рассмотрение спецификации, используемые для верификации.

</div><div class="col">

![](figures/actualization-graph-adv.png)

</div></div>

----

### Модель-процесс-вычислитель

<div class="row"><div class="col">

- Модель (конфигурация) вычислительного процесса, описывающая вычислительный процесс в рамках ВПл.
- Вычислитель целостный зафиксированный механизм, обеспечивающий вычислительный процесс.
- Вычислительный процесс (ВП) и частичный процесс, который разворачивается вычислителем и соответствует модели по построению или формальному критерию.
- Компонент ВП -- атомарный или составной шаг вычислителя.

</div><div class="col">

![](figures/mvp-1.png)

</div></div>

----

<div class="row"><div class="col">

- Вычислительный механизм (ВМх) -- часть вычислителя, обеспечивающая компонент ВП.
- Отношение виртуализации -- абстракция над процессом, фиксирующая вычислитель.
    - Определяет полное множество атомарных компонент ВП и позволяет описать любой допустимый ВП.
    - Невыразимость процесса говорит о сбое вычислителя или некорректной виртуализации.
- Отношение трансляции -- формальное соответствие двух моделей друг-другу.

</div><div class="col">

![](figures/mvp-2.png)

</div></div>

---

## Архитектурное решение

1. Наименование проблемы
1. Решение ...
1. Статус: `pending` | `decided` | `approved`
1. Группа: ...
1. Предположения
1. Альтернативы
1. Аргументы
1. Последствия
1. Связанные решения
1. Связанные требования
1. Связанные артефакты
1. Заметки

----

### Наименование проблемы

Issue. State the architectural design issue being addressed.

This should leave no questions about the reason why this issue is to be addressed now.

### Решение

Decision. Clearly state the solution chosen. It is the selection of one of the positions that the architect could have taken.

### Статус: pending | decided | approved

Status. State the status of the decision, such as pending, decided, or approved. (This is not the status of implementing the decision.)

----

### Группа

Group. Name a containing group. Grouping allows for filtering based on the technical stakeholder interests. A simple group label, such as “integration,” “presentation,” “data,” and so on can be used to help organize the set of decisions. For example, the data architects reviewing the decisions can focus only on the decisions classified as data.

### Предположения

Assumptions. Clearly describe the underlying assumptions in the environment in which a decision is being made. These could be cost, schedule, technology, and so on. Note that constraints in the environment (such as a list of accepted technology standards, an enterprise architecture, or commonly employed patterns) may limit the set of alternatives considered.

----

### Альтернативы

Alternatives. List alternatives (that is, options or positions) considered. Explain alternatives with sufficient detail to judge their suitability; refer to external documentation to do so if necessary. Only viable positions should be described here. While you don’t need an exhaustive list, you also don’t want to hear the question “Did you think about . . . ?” during a final review, which might lead to a loss of credibility and a questioning of other architectural decisions. Listing alternatives espoused by others also helps them know that their opinions were heard. Finally, listing alternatives helps the architect make the right decision, because listing alternatives cannot be done unless those alternatives were given due consideration.

----

### Аргументы

Argument. Outline why a position was selected. This is probably as important as the decision itself. The argument for a decision can include items such as implementation cost, total cost of ownership, time to market, and availability of required development resources.

### Последствия

Implications. Describe the decision’s implications. For example:

- Introduce a need to make other decisions
- Create new requirements
- Modify existing requirements
- Pose additional constraints to the environment
- Require renegotiation of scope
- Require renegotiation of the schedule with the customers
- Require additional training for the staff

----

### Связанные решения

Related Decisions. List decisions related to this one. A traceability matrix or decision tree is useful, as is showing complex relations diagrammatically such as with object models. Useful relations among decisions include causality (which decisions caused other ones), structure (showing decisions’ parents or children, corresponding to architecture elements at higher or lower levels), or temporality (which decisions came before or after others).

### Связанные требования

Related Requirements. Map decisions to objectives or requirements, to show accountability. Each architecture decision is assessed as to its contribution to each major objective. We can then assess how well the objective is met across all decisions, as part of an overall architecture evaluation.

----

### Связанные артефакты

Affected Artifacts. List the architecture elements and/or relations affected by this decision. You might also list the effects on other design or scope decisions, pointing to the documents where those decisions are described. You might also include external artifacts upstream and downstream of the architecture, as well as management artifacts such as budgets and schedules.

### Заметки

Notes. Capture notes and issues that are discussed during the decision process.

---

## Архитектурное представление

<div class="row"><div class="col">

1. Наименование представления
1. Основное представление
1. Каталог элементов
    - Элементы и их свойства
    - Отношения и их свойства
    - Интерфейсы элементов
    - Поведение элементов
1. Диаграмма контекста
1. Инструкции по вариативности
1. Обоснование

</div><div class="col">

![](figures/arch-view-template.png)

</div></div>

----

### Наименование представления

### Основное представление

The **primary presentation** shows the elements and relations of the view. The primary presentation should contain the information you wish to convey about the system -- in the vocabulary of that view -- first. It should certainly include the primary elements and relations, but under some circumstances might not include all of them.

----

### Каталог элементов

The **element catalog** details at least those elements depicted in the primary presentation. For instance, if a diagram shows elements A, B, and C, then the element catalog needs to explain what A, B, and C are and their purposes or the roles they play, rendered in the vocabulary of the view. In addition, if elements or relations relevant to this view were omitted from the primary presentation, they should be introduced and explained in the catalog. Specific parts of the catalog include the following:

----

#### Элементы и их свойства

**Elements and their properties**. This section names each element in the view and lists the properties of that element. Each style introduced throughout Part I listed a set of suggested properties associated with that style. For example, elements in a decomposition view might have the property of “responsibility”--an explanation of each module’s role in the system--and elements in a communicating-processes view might have timing parameters, among other things, as properties. Whether the properties are generic to the style chosen or the architect has introduced new ones, this is where they are documented and given values.

----

#### Отношения и их свойства

**Relations and their properties**. Each view has specific relation type(s) that it depicts among the elements in that view. Mostly, these relations are shown in the primary presentation. However, if the primary presentation does not show all the relations, or if there are exceptions to what is depicted in the primary presentation, this is the place to record that information. Otherwise, this section will be empty.

#### Интерфейсы элементов

**Element interfaces**. This section documents element interfaces.

#### Поведение элементов

**Element behavior**. Some elements have complex interactions with their environment. For purposes of understanding or analysis, it is incumbent on the architect to specify element behavior.

----

### Диаграмма контекста

A **context diagram** shows how the system or portion of the system depicted in this view relates to its environment.

### Инструкции по вариативности

A **variability guide** shows how to exercise any variation points that are a part of the architecture shown in this view.

### Обоснование

**Rationale** explains why the design reflected in the view came to be. The goal of this section is to explain why the design is as it is and to provide a convincing argument that it is sound. The use of a pattern or style in this view should be justified here.
